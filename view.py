import sys
import time

from PyQt5.uic import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

TASK_HEADER_WIDTH	=	220

class View(QMainWindow):
	lb_building		=	[]
	def __init__(self):
		super(View, self).__init__()
		loadUi('gui.ui', self)
		self.setWindowTitle('Travian Butler')

		self.lb_building	=  [self.lb_res_1,i		self.lb_res_2,		self.lb_res_3,		self.lb_res_4,
								self.lb_res_5,		self.lb_res_6,		self.lb_res_7,		self.lb_res_8,
								self.lb_res_9,		self.lb_res_10,		self.lb_res_11,		self.lb_res_12,
								self.lb_res_13,		self.lb_res_14,		self.lb_res_15,		self.lb_res_16,
								self.lb_res_17,		self.lb_res_18,		self.lb_res_19,		self.lb_res_20,
								self.lb_res_21,		self.lb_res_22,		self.lb_res_23,		self.lb_res_24,
								self.lb_res_25,		self.lb_res_26,		self.lb_res_27,		self.lb_res_28,
								self.lb_res_29,		self.lb_res_30,		self.lb_res_31,		self.lb_res_32,
								self.lb_res_33,		self.lb_res_34,		self.lb_res_35,		self.lb_res_36,
								self.lb_res_37,		self.lb_res_38,		self.lb_res_39,		self.lb_res_40]

		self.lb_background_outer.setPixmap(QPixmap('img/dorf1.jpg'))
		self.lb_background_inner.setPixmap(QPixmap('img/dorf2.jpg'))

		self.tw_current_built.horizontalHeader().setSectionResizeMode(QHeaderView.Fixed)
		self.tw_current_built.horizontalHeader().resizeSection(0, TASK_HEADER_WIDTH)
		self.tw_current_built.horizontalHeader().resizeSection(1, self.tw_current_built.width() - TASK_HEADER_WIDTH - 2)

		self.tw_current_built.clearContents()

	def update_combobox(self, building_list):
		self.cb_building_list.clear()

		for building in building_list:
			self.cb_building_list.addItem("{}. {} Level {}".format(building.id, building.name, building.level))

	def update_label_building_list(self, building_list):
		try:
			for i in range (0, 18):
				self.lb_building[i].setText(str(building_list[i].level))
		except IndexError:
			print (i)
		for i in range (18, 40):
			self.lb_building[i].setText("{} {}".format(building_list[i].name, building_list[i].level))

	def update_progressbar_resources(self, storage, capacity):
		self.pb_lumber.setValue (storage['lumber'] 	* 100 / capacity['lumber'])
		self.pb_clay.setValue	(storage['clay'] 	* 100 / capacity['clay'])
		self.pb_iron.setValue	(storage['iron'] 	* 100 / capacity['iron'])
		self.pb_crop.setValue	(storage['crop'] 	* 100 / capacity['crop'])

	def update_table_current_built(self, current_built_list):
		COLUMN_TASK			=	0
		COLUMN_TIME_LEFT	=	1


		self.tw_current_built.clearContents()

		if (len(current_built_list) != 0):
			for index in range(len(current_built_list)):

				text_task			=	"{}    Level {}".format(current_built_list[index].name, current_built_list[index].level)
				text_time_left		=	time.strftime("%H:%M:%S", time.gmtime(current_built_list[index].duration_in_sec))

				task				=	QTableWidgetItem(text_task)
				time_left			=	QTableWidgetItem(text_time_left)

				self.tw_current_built.setItem(index, COLUMN_TASK, task)
				self.tw_current_built.setItem(index, COLUMN_TIME_LEFT, time_left)

				self.tw_current_built.item(index, COLUMN_TIME_LEFT).setTextAlignment(Qt.AlignCenter)

	def update_label_adventure(self, new_adventures):

		if (new_adventures == 0):
			self.lb_adventure.setText("No new adventures")
		elif (new_adventures == 1):
			self.lb_adventure.setText("1 new adventure")
		else:
			self.lb_adventure.setText("{} adventures".format(new_adventures))

	def update_task_list(self, building):
		self.lw_task_list.addItem(building)

	def task_list_remove_item(self):
		self.lw_task_list.takeItem(self.lw_task_list.currentRow())
		self.lw_task_list.setCurrentRow(-1)

# app = QApplication(sys.argv)
# main_window = App()

# main_window.show()

# sys.exit(app.exec_())