import re
import time
import sys
import requests

from collections import *

SEC_PER_HOUR			=	3600

class Building(object):
	def __init__(self, name = '', level = 0, id = 0, url = ''):
		self.name 					= 	name
		self.level					=	int (level)
		self.id						=	int (id)
		self.url					=	url
		self.pending_level			=	0

		if (self.id < 18):
			self.location	=	"OUTER"
		else:
			self.location	=	"INNER"

	# dung timer de wait duoc, timer de o ngoai, pass qua test cua timer roi moi vao den day
	def upgrade_max_level(self, request, URL_ORIGIN):
		is_max_leveled	=	False
		try:
			while not (is_max_leveled): 
				page 	=	request.get(self.url)
				if ("Construct with master builder" in page.text):
					is_busy			=	True
					pass
				elif ("reached max level" in page.text):
					is_max_leveled	=	True
					break
				else:
					is_busy 		=	False
					token			=	Model.get_build_token(self.location, page.text)
					self.level		=	Model.get_building_level(page.text)

					page			=	request.get(URL_ORIGIN + token)
				
				if (is_busy):
					time.sleep(1)
					#muon dung sleep o day thi phai dung thread o ngoai di khong treo
				else:
					time.sleep(Model.get_build_time(page.text))									
		except IndexError as e:
		# raise e
			print("Bug")
			print(self.url)
			print(self.name)
		# write_html_log(page.text)		

	def upgrade_one_level(self, request, URL_ORIGIN):
			page		=	request.get(self.url)

			if ("reached max level" in page.text):
				return

			token		=	Model.get_build_token(self.location, page.text)
			if (token	==	''):
				return
			self.level	=	Model.get_building_level(page.text)

			request.get(URL_ORIGIN + token)

			self.pending_level -= 1

class Current_Built(Building):
	def __init__(self, name = '', level = 0, duration_in_sec = -1):
		super(Current_Built, self).__init__(name = name, level = level)

		self.duration_in_sec 	=	int(duration_in_sec)

	def update(self, name = None, level = None, duration_in_sec = None):

		if (name 			!= 	None):
			self.name				=	name

		if (level 			!= 	None):
			self.level				=	level

		if (duration_in_sec	!=	None):
			self.duration_in_sec	=	duration_in_sec

class Logical_Functions:
	@staticmethod
	def get_build_token (LOCATION, page_content):

		try:
			if (LOCATION == "OUTER"):
				token = re.findall(r"(?<=')dorf1\.php.+?(?=')", page_content)[0]
			else:
				token = re.findall(r"(?<=')dorf2\.php.+?(?=')", page_content)[0]

			if ("&b=" in token):
				return ''
			else:
				return token.replace("&ins","")
		except IndexError:
			return ''

	@staticmethod
	def get_build_time (page_content):
		result = re.findall(r"alt=\"duration\">\n(.+?) </span>", page_content)
		time = result[0].split(':')
		time_in_sec = int(time[0]) * 60 * 60 + int(time[1]) * 60 + int(time[2])
		return time_in_sec + 1

	####################################

	@staticmethod
	def get_building_level (page_content):
		raw_data 	= 	re.findall(r'titleInHeader">(.*?)</span>', page_content)[0]
		# name 		= 	re.findall(r'(.+?) <span', result)[0]
		level 		= 	re.findall(r'level (\d+)',raw_data)[0]
		
		return int(level)

	@staticmethod
	def get_all_building_info (page_content, URL_ORIGIN):
		outer_buildings = []

		raw_data 		=	re.findall(r'<area(.+?)/>', page_content, re.DOTALL)

		try:
			for line in raw_data:
				if ('data-index' not in line): #last <area> tag in dorf1.php
					continue
				elif ('Building site' in line):
					name 	=	'Building site'
					level	=	0
				elif ('Rallypoint site' in line):
					name 	=	'Rallypoint site'
					level	=	0
				else:
					name	=	re.findall(r'title="(.+?)&amp;', line)[0]
					level	=	re.findall(r'level (.+?)&lt;', line)[0]

				id 			= 	re.findall(r'data-index="(.+?)"', line)[0]
				url			=	re.findall(r'href="(.+?)"', line)[0]

				outer_buildings.append(Building(name, level, id, URL_ORIGIN + url))

			return outer_buildings
		except IndexError:
			write_html_log(page_content)

	####################################

	@staticmethod
	def get_demolish_data (building_id, page_content):
		a 				= 	re.findall(r'name="a" value="(.+?)"', page_content)
		c 				= 	re.findall(r'name="c" value="(.+?)"', page_content) 
		data_demolish 	= 	dict(	gid = 15,
									a = a[0],
									c = c[0],
									abriss = building_id)
		return data_demolish

	####################################

	@staticmethod
	def get_send_troops_data (t1_amount, dname, option, page_content):

		timestamp 			= 	re.findall(r'name="timestamp" value="(\d+)', 			page_content)
		timestamp_checksum 	= 	re.findall(r'name="timestamp_checksum" value="(.+?)"',	page_content)
		b 					= 	re.findall(r'name="b" value="(.+?)"', 					page_content)
		current_did 		= 	re.findall(r'name="currentDid" value="(.+?)"', 			page_content)
		mpvt_token 			= 	re.findall(r'name="mpvt_token" value="(.+?)"', 			page_content)

		if (option == "raid"):
			c = 4
		elif (option == "attack"):
			c = 3
		elif (option == "reinforcement"):
			c = 2

		data_send_troops 	= 	dict(	timestamp 			= 	timestamp[0], 
										timestamp_checksum 	= 	timestamp_checksum[0],
										b 					= 	b[0],
										currentDid 			= 	current_did[0],
										mpvt_token 			= 	mpvt_token[0],
										t1 					= 	t1_amount,
										t4 					= 	'',
										t7 					= 	'',
										t9 					= 	'',
										t2 					= 	'',
										t5 					= 	'',
										t8 					= 	'',
										t10 				= 	'',
										t3					= 	'',
										t6 					= 	'',
										# t11 				= 	'',
										dname 				= 	dname,
										x					= 	'',
										y 					= 	'',
										c 					= 	c,
										s1 					= 	'ok')

		return data_send_troops

	@staticmethod
	def get_send_troops_confirm_data (page_content):
		# redeploy_hero 		= 	re.findall(r"name=\"redeployHero\" value=\"(.*?)(?=\" )", 		page_content)
		timestamp 			= 	re.findall(r'name="timestamp" value="(.+?)"', 			page_content)
		timestamp_checksum	= 	re.findall(r'name="timestamp_checksum" value="(.+?)"', 	page_content)
		ID 					= 	re.findall(r'name="id" value="(.+?)"', 					page_content)
		a 					= 	re.findall(r'name="a" value="(.+?)"', 					page_content)
		c 					= 	re.findall(r'name="c" value="(.+?)"', 					page_content)
		kid 				= 	re.findall(r'name="kid" value="(.+?)"', 				page_content)
		t1 					= 	re.findall(r'name="t1" value="(.+?)"', 					page_content)
		t2 					= 	re.findall(r'name="t2" value="(.+?)"', 					page_content)
		t3 					= 	re.findall(r'name="t3" value="(.+?)"', 					page_content)
		t4 					= 	re.findall(r'name="t4" value="(.+?)"', 					page_content)
		t5 					= 	re.findall(r'name="t5" value="(.+?)"', 					page_content)
		t6 					= 	re.findall(r'name="t6" value="(.+?)"', 					page_content)
		t7 					= 	re.findall(r'name="t7" value="(.+?)"', 					page_content)
		t8 					= 	re.findall(r'name="t8" value="(.+?)"', 					page_content)
		t9 					= 	re.findall(r'name="t9" value="(.+?)"', 					page_content)
		t10 				= 	re.findall(r'name="t10" value="(.+?)"', 				page_content)
		t11 				= 	re.findall(r'name="t11" value="(.+?)"', 				page_content)
		send_really 		= 	re.findall(r'name="sendReally" value="(.+?)"', 			page_content)
		troop_sent 			= 	re.findall(r'name="troopsSent" value="(.+?)"', 			page_content)
		current_did 		= 	re.findall(r'name="currentDid" value="(.+?)"', 			page_content)
		b 					= 	re.findall(r'name="b" value="(.+?)"', 					page_content)
		dname 				= 	re.findall(r'name="dname" value="(.*?)"', 				page_content)
		x 					= 	re.findall(r'name="x" value="(.+?)"', 					page_content)
		y 					= 	re.findall(r'name="y" value="(.+?)"', 					page_content)
		
		data_confirm		= 	dict(	redeployHero 		= 	'',
										# redeployHero 		= 	redeploy_hero[0],
										timestamp 			=	timestamp[0],
										timestamp_checksum 	= 	timestamp_checksum[0],
										id 					= 	ID[0],
										a 					= 	a[0],
										c 					= 	c[0],
										kid 				= 	kid[0],
										t1 					= 	t1[0],
										t2 					= 	t2[0],
										t3 					= 	t3[0],
										t4 					= 	t4[0],
										t5 					= 	t5[0],
										t6 					= 	t6[0],
										t7 					= 	t7[0],
										t8 					= 	t8[0],
										t9 					= 	t9[0],
										t10 				= 	t10[0],
										t11 				= 	t11[0],
										sendReally 			= 	send_really[0],
										troopsSent 			= 	troop_sent[0],
										currentDid 			= 	current_did[0],
										b 					=	b[0],
										dname 				= 	dname[0],
										x 					= 	x[0],
										y 					= 	y[0],
										s1 					= 	'ok')

		return data_confirm

	####################################

	@staticmethod
	def get_smithy_info (page_content):
		smithy_info = dict()

		result = re.findall(r'<div class="title">(.+?)</span>', page_content, re.DOTALL)
		for raw in result:
			name = re.findall(r'title="(.+?)"', raw)
			level = re.findall(r'level (\d+)', raw)
			smithy_info[name[0]] = level[0]

		return smithy_info

	@staticmethod
	def get_smithy_token (page_content):
		result = re.findall(r"build\.php\?id=23&a=.+(?=')", page_content)
		#build.php?id=23&a=1&c=a8a4f3b4
		return(result[0])

	####################################

	@staticmethod
	def get_adventure_token (page_content): 
		
		token 	= 	re.findall(r'class="gotoAdventure arrow" href="(.+?)"', page_content)
		#<a class="gotoAdventure arrow" href="start_adventure.php?skip=1&amp;from=list&amp;kid=86496">To the adventure.</a>
		time 	= 	re.findall(r'<td class="moveTime"> (.+?) </td>', page_content)
		
		return(token[0].replace("&amp;","&"))

	@staticmethod
	def get_adventure_data (page_content):
		send 	= 	re.findall(r'name="send" value="(.+?)"', page_content)
		kid 	= 	re.findall(r'name="kid" value="(.+?)"', page_content)
		a 		= 	re.findall(r'name="a" value="(.+?)"', page_content)

		adventure_data = dict(	send 	= 	send[0], 
								kid 	= 	kid[0],
								a 		= 	a[0], 
								start 	= 'Start adventure')
		adventure_data['from'] = 'list'
		
		return adventure_data

	@staticmethod
	def get_new_adventures(page_content):
		raw_data				=	re.findall(r'class="layoutButton adventure(.+?)</button>', page_content, re.DOTALL)[0]

		try:
			new_adventures		=	re.findall(r'class="speechBubbleContent">(.+?)</div>', raw_data)[0]
		except(IndexError):
			new_adventures		=	0

		return int(new_adventures)

	####################################

	@staticmethod
	def get_village_list (server_address, page_content):
		id_village			=	re.findall(r'href="\?newdid=(.+?)"', page_content)
		url_village			=	[]

		for id in id_village:
			url_village.append("{}dorf2.php?newdid={}".format(server_address, id))

		return url_village

	####################################

	@staticmethod
	def get_resources_production_info (page_content):
		production 				=	{}

		raw_data				=	re.findall(r'resources.production = {\n(.+?)}', page_content, re.DOTALL)[0]

		production['lumber']	=	int (re.findall(r'"l1": (.+?),', raw_data)[0])
		production['clay'] 		=	int (re.findall(r'"l2": (.+?),', raw_data)[0])
		production['iron'] 		=	int (re.findall(r'"l3": (.+?),', raw_data)[0])
		production['crop'] 		=	int (re.findall(r'"l4": (.+?),', raw_data)[0])

		return production

	@staticmethod
	def get_resources_capacity_info (page_content):
		capacity				=	{}

		raw_data				=	re.findall(r'resources.maxStorage = {\n(.+?)}', page_content, re.DOTALL)[0]

		capacity['lumber']		=	int (re.findall(r'"l1": (.+?),', raw_data)[0])
		capacity['clay'] 		=	int (re.findall(r'"l2": (.+?),', raw_data)[0])
		capacity['iron'] 		=	int (re.findall(r'"l3": (.+?),', raw_data)[0])
		capacity['crop'] 		=	int (re.findall(r'"l4": (.+?)$', raw_data)[0])

		return capacity

	@staticmethod
	def get_resources_storage_info (page_content):
		storage				=	{}

		raw_data			=	re.findall(r'resources.storage = {\n(.+?)}', page_content, re.DOTALL)[0]

		storage['lumber']	=	int (re.findall(r'"l1": (.+?)\.', raw_data)[0])
		storage['clay']		=	int (re.findall(r'"l2": (.+?)\.', raw_data)[0])
		storage['iron']		=	int (re.findall(r'"l3": (.+?)\.', raw_data)[0])
		storage['crop']		=	int (re.findall(r'"l4": (.+?)\.', raw_data)[0])

		return storage

	@staticmethod
	def get_build_duration(page_content):
		if ('buildDuration' in page_content):
			raw_data				=	re.findall(r'<div class="buildDuration">(.+?)</div>', page_content, re.DOTALL)[0]
			build_duration_in_sec 	=	int (re.findall(r'value="(.+?)"', raw_data)[0])
			
			return build_duration_in_sec
		else :
			return -1
	
	@staticmethod
	def get_current_built_info (page_content):
		current_built_list			=	deque([])

		if ('buildDuration' in page_content):
			raw_data		=	re.findall(r'<div class="boxes buildingList">(.+)End at', page_content, re.DOTALL)[0]

			name 			=	re.findall(r'<div class="name">\n(.+?)\n<span class="lvl">', raw_data, re.DOTALL)
			level			= 	re.findall(r'<span class="lvl">level (.+?)</span>', raw_data)
			build_duration_in_sec 	=	re.findall(r'counting="down" value="(.+?)"', raw_data)

			for i in range(len(name)):
				current_built_list.append(Current_Built(name 			 = 	name[i].strip(), 
														level 			 = 	level[i],
														duration_in_sec  = 	build_duration_in_sec[i]))

		return current_built_list

class Model(Logical_Functions):
	#nhung cai self.model.url nay se phai chuyen tu edit source code sang nhap tay

	URL_VILLAGE				=	['']
	URL_LOGIN 				=	"https://tx16.citvian.org/login.php"
	URL_BUILDING			=	"https://tx16.citvian.org/build.php?id="
	URL_OUTER				=	"https://tx16.citvian.org/dorf1.php"
	URL_INNER				=	"https://tx16.citvian.org/dorf2.php"
	URL_ORIGIN 				=	"https://tx16.citvian.org/"
	URL_MAIN_BUILDING		=	"https://tx16.citvian.org/build.php?id=26"
	URL_DEMOLISH			=	"https://tx16.citvian.org/build.php?gid=15"
	URL_ADVENTURE			=	"https://tx16.citvian.org/hero.php?t=3"

	storage					=	{}
	capacity				=	{}
	production_per_hour		=	{}

	building_list			=	[]
	current_built_list		=	deque([])
	new_adventures			=	0

	rq 						= 	requests.Session()
	
	
	def __init__(self):
		super(Model, self).__init__()

	def update_current_built_info(self, page = ''):
		if	(page			== 	''):
			page			= 	self.rq.get(self.URL_OUTER)

		self.current_built_list	=	self.get_current_built_info(page.text)

		# for building in current_built_list:
		# 	building.duration_in_sec	=	self.get_build_duration(page.text)

		# self.current_built.update(	name 				= 	temp_building.name, 
		# 							level 				= 	temp_building.level,
		# 							duration_in_sec 	= 	self.get_build_duration(page.text))

	def update_resources_info(self, page = ''):
		if (page	== 	''):
			page					=	self.rq.get(self.URL_OUTER)

		self.storage 				= 	self.get_resources_storage_info(page.text)
		self.capacity				=	self.get_resources_capacity_info(page.text)
		self.production_per_hour	=	self.get_resources_production_info(page.text)

	def update_building_list_info(self, page_outer = '', page_inner = ''):
		if (page_outer 		== 	''):
			page_outer		= 	self.rq.get(self.URL_OUTER)

		self.building_list 	=	self.get_all_building_info(page_outer.text, self.URL_ORIGIN)

		if (page_inner 		== 	''):
			page_inner		=	self.rq.get(self.URL_INNER)

		self.building_list	+=	self.get_all_building_info(page_inner.text, self.URL_ORIGIN)	

	def update_adventure(self, page = ''):
		if (page	== 	''):
			page			=	self.rq.get(self.URL_OUTER)

		self.new_adventures	=	self.get_new_adventures(page.text)

	def update_storage(self):
		for resources_type, value in self.storage.items():
			self.storage[resources_type] 		+= 	self.production_per_hour[resources_type] / SEC_PER_HOUR

			if (self.storage[resources_type]	> 	self.capacity[resources_type]):
				self.storage[resources_type] 	= 	self.capacity[resources_type]

	def go_adventure(self):
		page	=	self.rq.get(self.URL_ADVENTURE)
		token	=	self.get_adventure_token(page.text)

		page	=	self.rq.get(self.URL_ORIGIN + token)
		data	=	self.get_adventure_data(page.text)

		self.rq.post("https://tx16.citvian.org/start_adventure.php", data = data)

	####################################

	def login(self):
		data_login 			= 	dict(user = 'SuKu', pw = 'tam1509', s1 = 'Login', w ='1920%3A1080', login = '1530296027')
		self.rq.post(self.URL_LOGIN, data = data_login)

		# page 				= 	self.rq.get(self.URL_OUTER)
		page 				= 	self.rq.get('https://tx16.citvian.org/dorf1.php?newdid=91248')
		self.URL_VILLAGE	+= 	self.get_village_list(self.URL_ORIGIN, page.text)	

	
			# write_html_log(page.text)


def write_html_log(page_content):
	with open('out.html', 'w') as f:
		f.write(page_content)
		f.close()